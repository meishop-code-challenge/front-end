![demo screens](/assets/demo/screens.png "Demo")

# Teste Front-End Meishop

Bem vindo dev! A ideia deste teste é medir seu nível de conhecimento atual e acima de tudo sua velocidade de aprendizado.

Está é uma aplicação simples de 3 telas, mas sinta-se livre quanto ao design, telas e funcionalidades, sua criatividade é o limite!

## Instruções

Deve ser feito uma aplicação com no mínimo 3 telas, sendo elas cadastro, login e pagina inicial.
Sinta-se a vontade para fazer modificações, adicionar funcionalidades e tratativas que ache necessário, mas lembre-se, menos pode ser mais as vezes.

- Cadastro: O usuário insere os dados requisitados e se cadastra no sistema

- Login: O usuário faz acesso ao sistema com os dados cadastrados

- Pagina inicial: A primeira página que o usuário deve ver ao fazer login na aplicação

## Cadastro

A tela de cadastro deve possuir um formulário com os campos nome, email, data de nascimento e senha, além de um botão para que o usuário possa realizar o cadastro e salvar os dados na aplicação.

## Login

Também com um formulário, mas com os campos de email e senha para que o usuário cadastrado possa acessar sua conta e ir para a página inicial.

## Pagina inicial

Deve ser exibida apenas depois que o usuário esteja logado na aplicação e funcionará como um dashboard listando os serviços da aplicação.

O dashboard deve possuir no mínimo 3 serviços, sendo 2 deles obrigatoriamente "Minha conta" e "Github" e o terceiro a sua escolha.
O serviço "Github" deve obrigatóriamente redirecionar o usuário para o seu github (assim podemos ver alguns dos seus outros projetos também) e o serviço "Minha conta" que deve redirecionar o usuário para uma tela aonde ele possa acessar todos os dados da conta dele e caso queira, possa alterar alguns dados.

## Minha conta

O usuário deve poder visualizar e editar todos os seus dados cadastrados.

## Tecnologias

Recomendamos utilzar [React Native](https://reactnative.dev) para o desenvolvimento deste teste juntamente com o [Expo](https://expo.dev).

### Opcionais:

- [NativeBase](https://docs.nativebase.io): Uma biblioteca de componentes que facilidata a estilização de componentes e responsividade do seu App.
- [Redux](https://redux.js.org) ou [Recoil](https://recoiljs.org): Gerenciamento de estados da aplicação de forma global.

## Recomendações

- Código limpo
- Boa estrutura de código
- Componentes reutilizaveis

## Design

### Assets

A logo deste projeto está disponivel na pasta "assets", neste mesmo repositório.

### Paleta de cores:

```#C15F2C```
```#EE754C```
```#000000```
```#F9F9F9```
